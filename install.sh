#!/bin/bash

# Remove Firewall D

echo "Stop and Remove Firewall D"
sleep 5


sudo systemctl stop firewalld
sudo systemctl disable firewalld

# Mask the FirewallD service which will prevent the firewall from being started by other services:
sudo systemctl mask --now firewalld

### Installing K3s
echo "Installing K3s"
sleep 5

curl -sfL https://get.k3s.io | sh -

sleep 10

# Change the k3 ownership form root to user
echo "Changing Kube-config ownership to user"

sleep 5
sudo chown -R $USER /etc/rancher/k3s/k3s.yaml


sleep 5

echo "Install Docker"

sleep 10

sudo yum install -y yum-utils

sudo yum-config-manager --add-repo https://download.docker.com/linux/centos/docker-ce.repo

sudo yum update -y

sudo yum install docker-ce docker-ce-cli -y

sudo systemctl start docker

sudo systemctl enable docker


sleep 10

echo "Configure Flash Docker Registry"

sudo docker login -u flashregistry1 -p ermF3tbsn=DXVCAMOZHY1UpSbcIDPSJ2 flashregistry1.azurecr.io

sleep 5
echo "Install git"

sleep 5

sudo yum install -y git


echo "Configure kubernetes to use Docker Registry"
sleep 10
kubectl create secret docker-registry flash-key \
 --docker-server=flashregistry1.azurecr.io \
 --docker-username=flashregistry1 \
 --docker-password=ermF3tbsn=DXVCAMOZHY1UpSbcIDPSJ2 \
 --docker-email=praise.adanlawo@deimos.co.za

sleep 5
echo "Install Kubernetes Demo app"

sleep 10

git clone https://gitlab.com/nerdeveloper/nodejs-demo.git

cd nodejs-demo

kubectl apply -f k8s/

sleep 2

echo "testing the kubernetes app"

sleep 5

echo "Testing service"

curl -i http://$(kubectl get svc | grep nodejsdemo |  awk '{print $3}')

sleep 5

echo "Testing Ingress via LoadBalancer"

curl -i http://$(kubectl get ingress | grep nodejsdemo |  awk '{print $4}')



